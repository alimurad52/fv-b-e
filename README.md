#Setup
1. Clone the repository in the `htdocs` folder of your MAMP/XAMPP.
2. Run the Apache server.
3. In your browser navigate to `http://localhost:8888/fv_test`.

#Assingment
1. You are new to Magento as you spoken on the phone, so can you give me a critique on how they built their frontend in terms of the architecture of how they built everything and also of the components they used ?

2. Magento 2 got quite a few things right, but then at the end of the day, it's not a perfect product. The backend is good but then the normal hindrance is the frontend that needs to be tweaked to get the best performance as well as it's a pain to develop on. So your second task is to build a frontend, using whatever you are familiar with (nodejs ?) that talks to a Magento instance's API you have set up with a few items in catalog. Firstly try using a default theme and display the items on the catalog - you can enjoy how slow it is :-), then write your own implementation of the frontend. It does not at this stage need to be able to checkout, just display with a nice frontend. 

#Explanation and Assumptions
Since it was my first time working with Magento, I followed the documentation provided [here](https://devdocs.magento.com/guides/v2.3/install-gde/bk-install-guide.html). I tried setting up Magento with XAMPP first, I ran into an issue related to `PHP Extension Intl`. I spent about 5hrs figuring that error out and then eventually moved to usimg MAMP. With MAMP there were no issues and the setup was pretty straigt forward. After setting up Magento, I looked into their [REST API](setup|https://devdocs.magento.com/guides/v2.3/rest/bk-rest.html) and was easily able to make calls to the API from my front-end. 

I have previously launced a [project](https://flofast.com) with Wordpress and WooCommerce so I was comparing Magento with that all along. In my opinion Magento is good as a backend but that itself isn't the most ideal backend to have as well. Its quite slow and heavy. Another thing was, maybe its 'cause it was my first time with Magento, its quite confusing and messy compared to Wordpress. The advantage Magento has over Wordpress/WooCommerce is the easy setup of REST API and no limitation to only use Magento front-end, unlike Wordpress. Whereas its about Magento front-end, I think it would be a better idea to have a separate front-end with one of the JS frameworks as there are more plugins/modules available for fron-end rather than Magento's front-end.